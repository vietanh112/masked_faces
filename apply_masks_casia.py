# This script applies masks over Casia-WebFace dataset. casia_folder, masks_folder, output folder should be set
# SecurifAI - 2021


import os
import posixpath as p
from glob import iglob
import shutil
import cv2
from tqdm import tqdm
import numpy as np
from PIL import Image, ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True


def overlay_image_alpha(img, img_overlay, pos, alpha_mask):
    x, y = pos

    y1, y2 = max(0, y), min(img.shape[0], y + img_overlay.shape[0])
    x1, x2 = max(0, x), min(img.shape[1], x + img_overlay.shape[1])

    y1o, y2o = max(0, -y), min(img_overlay.shape[0], img.shape[0] - y)
    x1o, x2o = max(0, -x), min(img_overlay.shape[1], img.shape[1] - x)

    if y1 >= y2 or x1 >= x2 or y1o >= y2o or x1o >= x2o:
        return

    channels = img.shape[2]

    alpha = alpha_mask[y1o:y2o, x1o:x2o]
    alpha_inv = 1.0 - alpha

    for c in range(channels):
        img[y1:y2, x1:x2, c] = (alpha * img_overlay[y1o:y2o, x1o:x2o, c] +
                                alpha_inv * img[y1:y2, x1:x2, c])




# casia images main folder
casia_folder = 'CASIA_WebFace'

# masks main folder
masks_folder = 'CASIA_WebFace_masks'

# masks img list
# mask_list = glob.glob(masks_folder + '/*/*.png')

# output folder
output_folder = 'masked_CASIA_WebFace'

mask_path_fail = 'mask_fail.txt'
f = open(mask_path_fail, 'a')

if p.isdir(output_folder):
    shutil.rmtree(output_folder)
os.mkdir(output_folder)
    
size = (112, 112)

for mask in tqdm(iglob(masks_folder + '/*/*.png')):    
    
    # get the basename of the mask to find original Casia image
    mask_basename = p.basename(mask)
    
    parent_folder = p.basename(p.dirname(mask))
    if not p.isdir(p.join(output_folder, parent_folder)):
        os.mkdir(p.join(output_folder, parent_folder))

    # create path for original mask
    img_path = p.join(casia_folder, parent_folder, mask_basename.replace('png','jpg')) 

    # check that original image exists, if it exists load it
    if p.isfile(img_path):
        casia_image = cv2.imread(img_path)
    else:
        f.write(img_path + ', img\n')
        print(img_path)
        continue
        
    # load the mask
    mask_image = cv2.imread(mask,cv2.IMREAD_UNCHANGED)
    if mask_image is None:
        print(mask)
        f.write(mask + ', mask\n')
        mask_image = cv2.cvtColor(np.array(Image.open(mask)), cv2.COLOR_RGBA2BGRA)

    # resize image to (112, 112)
    mask_image = cv2.resize(mask_image, size, interpolation=cv2.INTER_LINEAR)
    casia_image = cv2.resize(casia_image, size, interpolation=cv2.INTER_LINEAR)
    
    # apply mask over image
    overlay_image_alpha(casia_image, mask_image[:, :, 0:3], (0, 0), mask_image[:, :, 3] / 255.0)

    # save new overlayed image
    cv2.imwrite(p.join(output_folder, parent_folder, 
                       mask_basename.replace('png','jpg')), casia_image)
f.close()